<?php
/**
 * Created by Rick Dennison
 * Date:      8/2/22
 *
 * File Name: Router.php
 * Project:   Login-MVC-2022
 */

namespace Core;

Use Exception;

class Router
{
    /**
     * 1. Class variables
     *      a. array routes[]
     *      b. array params[]
     * 2. Main functions
     *      a. add(string route, params=[])
     *      b. match(string url): bool
     *      c. dispatch(string url)
     *      d. removeQueryStringVariables(string url):string
     * 3. Helper functions
     *      a. getRoutes(): array
     *      b. getParams(): array
     *      c. convertToStudlyCaps(string $string): string
     *      d. convertToCamelCase(string $string): string
     *      e. getNamespace(): string
     */

}