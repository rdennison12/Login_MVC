# Login MVC 2022

## Commits

* Initial commit - Repo created
* Project created with .gitignore
* .htaccess created
* Installed Twig templating and configured for psr-4
* Created all files and folders to create a basic MVC application
* Begin to write code for initial project
* Moved online repo to GitLab
