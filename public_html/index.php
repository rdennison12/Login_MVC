<?php
/**
 * Created by Rick Dennison
 * Date:      8/2/22
 *
 * File Name: index.php
 * Project:   Login-MVC-2022
 */

/**
 * Autoloader
 */


/**
 * Error and Exception handling
 */

/**
 * Sessions
 */

/**
 * Routing
 */

/**
 * Routing Table
 * To include routes for logging in and out, and dispatching URLs
 */